"""
    Fichier : gestion_contacts_personnes_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les contacts et les personnes.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : contacts_personnes_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /contacts_personnes_afficher
    
    But : Afficher les contacts avec les personnes associés pour chaque contact.
    
    Paramètres : id_personne_sel = 0 >> tous les contacts.
                 id_personne_sel = "n" affiche le contact dont l'id est "n"
                 
"""


@obj_mon_application.route("/contacts_personnes_afficher/<int:id_contact_sel>", methods=['GET', 'POST'])
def contacts_personnes_afficher(id_contact_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_contacts_personnes_afficher:
                code, msg = Exception_init_contacts_personnes_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_contacts_personnes_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_contacts_personnes_afficher.args[0]} , "
                      f"{Exception_init_contacts_personnes_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_personnes_contacts_afficher_data = """SELECT *, GROUP_CONCAT(telephone_contact) as personnescontacts FROM t_contact_personne
                                                            RIGHT JOIN t_personne ON t_personne.id_personne = t_contact_personne.fk_personne
                                                            LEFT JOIN t_contact ON t_contact.id_contact = t_contact_personne.fk_contact
                                                            GROUP BY id_personne"""
                if id_contact_sel == 0:
                    # le paramètre 0 permet d'afficher tous les contacts
                    # Sinon le paramètre représente la valeur de l'id du contact
                    mc_afficher.execute(strsql_personnes_contacts_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du contact sélectionné avec un nom de variable
                    valeur_id_contact_selected_dictionnaire = {"value_id_contact_selected": id_contact_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_personnes_contacts_afficher_data += """ HAVING id_personne= %(value_id_contact_selected)s"""

                    mc_afficher.execute(strsql_personnes_contacts_afficher_data, valeur_id_contact_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personnes_contacts_afficher = mc_afficher.fetchall()
                print("data_personnes ", data_personnes_contacts_afficher, " Type : ", type(data_personnes_contacts_afficher))

                # Différencier les messages.
                if not data_personnes_contacts_afficher and id_contact_sel == 0:
                    flash("""La table "t_contact" est vide. !""", "warning")
                elif not data_personnes_contacts_afficher and id_contact_sel > 0:
                    # Si l'utilisateur change l'id_contact dans l'URL et qu'il ne correspond à aucun contact
                    flash(f"Le contact {id_contact_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données contacts et personnes affichés !!", "success")

        except Exception as Exception_contacts_personnes_afficher:
            code, msg = Exception_contacts_personnes_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception contacts_personnes_afficher : {sys.exc_info()[0]} "
                  f"{Exception_contacts_personnes_afficher.args[0]} , "
                  f"{Exception_contacts_personnes_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("contacts_personnes/contacts_personnes_afficher.html", data=data_personnes_contacts_afficher)


"""
    nom: edit_personne_contact_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les personnes du contact sélectionné par le bouton "MODIFIER" de "contacts_personnes_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personne".
    2) Les personnes attribués au contact selectionné.
    3) Les personnes non-attribués au contact sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_personne_contact_selected", methods=['GET', 'POST'])
def edit_personne_contact_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_personnes_afficher = """SELECT id_personne, prenom_personne FROM t_personne ORDER BY id_personne ASC"""
                mc_afficher.execute(strsql_personnes_afficher)
            data_personnes_all = mc_afficher.fetchall()
            print("dans edit_personne_contact_selected ---> data_personnes_all", data_personnes_all)

            # Récupère la valeur de "id_contact" du formulaire html "contacts_personnes_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_contact"
            # grâce à la variable "id_personne_contacts_edit_html" dans le fichier "contacts_personnes_afficher.html"
            # href="{{ url_for('edit_personne_contact_selected', id_personne_contacts_edit_html=row.id_contact) }}"
            id_contact_personnes_edit = request.values['id_personne_contacts_edit_html']

            # Mémorise l'id du contact dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_contact_personnes_edit'] = id_contact_personnes_edit

            # Constitution d'un dictionnaire pour associer l'id du contact sélectionné avec un nom de variable
            valeur_id_contact_selected_dictionnaire = {"value_id_personne_selected": id_contact_personnes_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction personnes_contacts_afficher_data
            # 1) Sélection du contact choisi
            # 2) Sélection des personnes "déjà" attribués pour le contact.
            # 3) Sélection des personnes "pas encore" attribués pour le contact choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personnes_contacts_afficher_data"
            data_personne_contact_selected, data_personnes_contacts_non_attribues, data_personnes_contacts_attribues = \
                personnes_contacts_afficher_data(valeur_id_contact_selected_dictionnaire)
            print(data_personne_contact_selected)
            lst_data_contact_selected = [item['id_personne'] for item in data_personne_contact_selected]
            print("lst_data_contact_selected  ", lst_data_contact_selected,
                  type(lst_data_contact_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui ne sont pas encore sélectionnés.
            lst_data_personnes_contacts_non_attribues = [item['id_contact'] for item in data_personnes_contacts_non_attribues]
            session['session_lst_data_personnes_contacts_non_attribues'] = lst_data_personnes_contacts_non_attribues
            print("lst_data_personnes_contacts_non_attribues  ", lst_data_personnes_contacts_non_attribues,
                  type(lst_data_personnes_contacts_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui sont déjà sélectionnés.
            lst_data_personnes_contacts_old_attribues = [item['id_contact'] for item in data_personnes_contacts_attribues]
            session['session_lst_data_personnes_contacts_old_attribues'] = lst_data_personnes_contacts_old_attribues
            print("lst_data_personnes_contacts_old_attribues  ", lst_data_personnes_contacts_old_attribues,
                  type(lst_data_personnes_contacts_old_attribues))

            print(" data data_personne_contact_selected", data_personne_contact_selected, "type ", type(data_personne_contact_selected))
            print(" data data_personnes_contacts_non_attribues ", data_personnes_contacts_non_attribues, "type ",
                  type(data_personnes_contacts_non_attribues))
            print(" data_personnes_contacts_attribues ", data_personnes_contacts_attribues, "type ",
                  type(data_personnes_contacts_attribues))

            # Extrait les valeurs contenues dans la table "t_personnes", colonne "prenom_personne"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_personne
            lst_data_personnes_contacts_non_attribues = [item['telephone_contact'] for item in data_personnes_contacts_non_attribues]
            print("lst_all_personnes gf_edit_personne_contact_selected ", lst_data_personnes_contacts_non_attribues,
                  type(lst_data_personnes_contacts_non_attribues))

        except Exception as Exception_edit_personne_contact_selected:
            code, msg = Exception_edit_personne_contact_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_personne_contact_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_personne_contact_selected.args[0]} , "
                  f"{Exception_edit_personne_contact_selected}", "danger")

    return render_template("contacts_personnes/contacts_personnes_modifier_tags_dropbox.html",
                           data_personnes=data_personnes_all,
                           data_contact_selected=data_personne_contact_selected,
                           data_personnes_attribues=data_personnes_contacts_attribues,
                           data_personnes_non_attribues=data_personnes_contacts_non_attribues)


"""
    nom: update_personne_contact_selected

    Récupère la liste de tous les personnes du contact sélectionné par le bouton "MODIFIER" de "contacts_personnes_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personne".
    2) Les personnes attribués au contact selectionné.
    3) Les personnes non-attribués au contact sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_personne_contact_selected", methods=['GET', 'POST'])
def update_personne_contact_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du contact sélectionné
            id_personne_selected = session['session_id_contact_personnes_edit']
            print("session['session_id_contact_personnes_edit'] ", session['session_id_contact_personnes_edit'])

            # Récupère la liste des personnes qui ne sont pas associés au contact sélectionné.
            old_lst_data_personnes_contacts_non_attribues = session['session_lst_data_personnes_contacts_non_attribues']
            print("old_lst_data_personnes_contacts_non_attribues ", old_lst_data_personnes_contacts_non_attribues)

            # Récupère la liste des personnes qui sont associés au contact sélectionné.
            old_lst_data_personnes_contacts_attribues = session['session_lst_data_personnes_contacts_old_attribues']
            print("old_lst_data_personnes_contacts_old_attribues ", old_lst_data_personnes_contacts_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme personnes dans le composant "tags-selector-tagselect"
            # dans le fichier "personnes_contacts_modifier_tags_dropbox.html"
            new_lst_str_personnes_contacts = request.form.getlist('name_select_tags')
            print("new_lst_str_personnes_contacts ", new_lst_str_personnes_contacts)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_contact_old = list(map(int, new_lst_str_personnes_contacts))
            print("new_lst_personne_contact ", new_lst_int_personne_contact_old, "type new_lst_personne_contact ",
                  type(new_lst_int_personne_contact_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_personne" qui doivent être effacés de la table intermédiaire "t_personne_contact".
            lst_diff_personnes_delete_b = list(
                set(old_lst_data_personnes_contacts_attribues) - set(new_lst_int_personne_contact_old))
            print("lst_diff_personnes_delete_b ", lst_diff_personnes_delete_b)

            # Une liste de "id_personne" qui doivent être ajoutés à la "t_personne_contact"
            lst_diff_personnes_insert_a = list(
                set(new_lst_int_personne_contact_old) - set(old_lst_data_personnes_contacts_attribues))
            print("lst_diff_personnes_insert_a ", lst_diff_personnes_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_contact"/"id_contact" et "fk_personne"/"id_personne" dans la "t_personne_contact"
            strsql_insert_personne_contact = """INSERT INTO t_contact_personne (id_contact_personne, fk_personne, fk_contact)
                                                    VALUES (NULL, %(value_fk_personne)s, %(value_fk_contact)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_contact" et "id_personne" dans la "t_personne_contact"
            strsql_delete_personne_contact = """DELETE FROM t_contact_personne WHERE fk_personne = %(value_fk_personne)s AND fk_contact = %(value_fk_contact)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le contact sélectionné, parcourir la liste des personnes à INSÉRER dans la "t_personne_contact".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_contact_ins in lst_diff_personnes_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du contact sélectionné avec un nom de variable
                    # et "id_contact_ins" (l'id du personne dans la liste) associé à une variable.
                    valeurs_contact_sel_personne_sel_dictionnaire = {"value_fk_contact": id_contact_ins,
                                                               "value_fk_personne": id_personne_selected}

                    mconn_bd.mabd_execute(strsql_insert_personne_contact, valeurs_contact_sel_personne_sel_dictionnaire)

                # Pour le contact sélectionné, parcourir la liste des personnes à EFFACER dans la "t_personne_contact".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_contact_del in lst_diff_personnes_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du contact sélectionné avec un nom de variable
                    # et "id_contact_del" (l'id du personne dans la liste) associé à une variable.
                    valeurs_contact_sel_personne_sel_dictionnaire = {"value_fk_contact": id_contact_del,
                                                               "value_fk_personne": id_personne_selected}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_personne_contact, valeurs_contact_sel_personne_sel_dictionnaire)

        except Exception as Exception_update_personne_contact_selected:
            code, msg = Exception_update_personne_contact_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_personne_contact_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_personne_contact_selected.args[0]} , "
                  f"{Exception_update_personne_contact_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_personne_contact",
    # on affiche les contacts et le(urs) personne(s) associé(s).
    return redirect(url_for('contacts_personnes_afficher', id_contact_sel=id_personne_selected))


"""
    nom: personnes_contacts_afficher_data

    Récupère la liste de tous les personnes du contact sélectionné par le bouton "MODIFIER" de "contacts_personnes_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des personnes, ainsi l'utilisateur voit les personnes à disposition

    On signale les erreurs importantes
"""

def personnes_contacts_afficher_data(valeur_id_personne_selected_dict):
    print("valeur_id_personne_selected_dict...", valeur_id_personne_selected_dict)
    try:

        strsql_personne_selected = """SELECT id_personne, prenom_personne, GROUP_CONCAT(id_contact) as contactspersonnes FROM t_contact_personne
                                        INNER JOIN t_personne ON t_personne.id_personne = t_contact_personne.fk_personne
                                        INNER JOIN t_contact ON t_contact.id_contact = t_contact_personne.fk_contact
                                        WHERE id_personne = %(value_id_personne_selected)s"""

        strsql_contacts_personnes_non_attribues = """SELECT id_contact, telephone_contact FROM t_contact WHERE id_contact not in(SELECT id_contact as idcontactspersonnes FROM t_contact_personne
                                                    INNER JOIN t_personne ON t_personne.id_personne = t_contact_personne.fk_personne
                                                    INNER JOIN t_contact ON t_contact.id_contact = t_contact_personne.fk_contact
                                                    WHERE id_personne = %(value_id_personne_selected)s)"""

        strsql_contacts_personnes_attribues = """SELECT id_personne, id_contact, telephone_contact FROM t_contact_personne
                                            INNER JOIN t_personne ON t_personne.id_personne = t_contact_personne.fk_personne
                                            INNER JOIN t_contact ON t_contact.id_contact = t_contact_personne.fk_contact
                                            WHERE id_personne = %(value_id_personne_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_contacts_personnes_non_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_contacts_personnes_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("contacts_personnes_afficher_data ----> data_contacts_personnes_non_attribues ", data_contacts_personnes_non_attribues,
                  " Type : ",
                  type(data_contacts_personnes_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_personne_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_contacts_personnes_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_contacts_personnes_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_contacts_personnes_attribues ", data_contacts_personnes_attribues, " Type : ",
                  type(data_contacts_personnes_attribues))

            # Retourne les données des "SELECT"
            return data_personne_selected, data_contacts_personnes_non_attribues, data_contacts_personnes_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans contacts_personnes_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans contacts_personnes_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_contacts_personnes_afficher_data:
        code, msg = IntegrityError_contacts_personnes_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans contacts_personnes_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_contacts_personnes_afficher_data.args[0]} , "
              f"{IntegrityError_contacts_personnes_afficher_data}", "danger")
