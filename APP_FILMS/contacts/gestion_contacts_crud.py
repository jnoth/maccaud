"""
    Fichier : gestion_contacts_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les contacts.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.contacts.gestion_contacts_wtf_forms import FormWTFAjouterContacts
from APP_FILMS.contacts.gestion_contacts_wtf_forms import FormWTFDeleteContact
from APP_FILMS.contacts.gestion_contacts_wtf_forms import FormWTFUpdateContact

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /contacts_afficher
    
    Test : ex : http://127.0.0.1:5005/contacts_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_contact_sel = 0 >> tous les contacts.
                id_contact_sel = "n" affiche le contact dont l'id est "n"
"""


@obj_mon_application.route("/contacts_afficher/<string:order_by>/<int:id_contact_sel>", methods=['GET', 'POST'])
def contacts_afficher(order_by, id_contact_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion contacts ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestioncontacts {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_contact_sel == 0:
                    strsql_contacts_afficher = """SELECT id_contact, telephone_contact, mail_contact FROM t_contact ORDER BY id_contact ASC"""
                    mc_afficher.execute(strsql_contacts_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_contact"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du contact sélectionné avec un nom de variable
                    valeur_id_contact_selected_dictionnaire = {"value_id_contact_selected": id_contact_sel}
                    strsql_contacts_afficher = """SELECT id_contact, telephopne_contact, mail_contact FROM t_contact WHERE id_contact = %(value_id_contact_selected)s"""

                    mc_afficher.execute(strsql_contacts_afficher, valeur_id_contact_selected_dictionnaire)
                else:
                    strsql_contacts_afficher = """SELECT id_contact, telephone_contact, mail_contact FROM t_contact ORDER BY id_contact DESC"""

                    mc_afficher.execute(strsql_contacts_afficher)

                data_contacts = mc_afficher.fetchall()

                print("data_contacts ", data_contacts, " Type : ", type(data_contacts))

                # Différencier les messages si la table est vide.
                if not data_contacts and id_contact_sel == 0:
                    flash("""La table "t_contact" est vide. !!""", "warning")
                elif not data_contacts and id_contact_sel > 0:
                    # Si l'utilisateur change l'id_contact dans l'URL et que le contact n'existe pas,
                    flash(f"Le contact demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_contact" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données contacts affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. contacts_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} contacts_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("contacts/contacts_afficher.html", data=data_contacts)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /contacts_ajouter
    
    Test : ex : http://127.0.0.1:5005/contacts_ajouter
    
    Paramètres : sans
    
    But : Ajouter un contact pour un film
    
    Remarque :  Dans le champ "name_contact_html" du formulaire "contacts/contacts_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/contacts_ajouter", methods=['GET', 'POST'])
def contacts_ajouter_wtf():
    form = FormWTFAjouterContacts()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion contacts ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestioncontacts {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_email_wtf = form.email_wtf.data
                name_telephone_wtf = form.telephone_wtf.data

                name_email_wtf = name_email_wtf.lower()
                name_telephone_wtf = name_telephone_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_email": name_email_wtf, "value_telephone": name_telephone_wtf}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_contact = """INSERT INTO t_contact (id_contact, mail_contact, telephone_contact) VALUES (NULL, %(value_email)s,%(value_telephone)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_contact, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('contacts_afficher', order_by='DESC', id_contact_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_contact_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_contact_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion contacts CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("contacts/contacts_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /contact_update
    
    Test : ex cliquer sur le menu "contacts" puis cliquer sur le bouton "EDIT" d'un "contact"
    
    Paramètres : sans
    
    But : Editer(update) un contact qui a été sélectionné dans le formulaire "contacts_afficher.html"
    
    Remarque :  Dans le champ "nom_contact_update_wtf" du formulaire "contacts/contact_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/contact_update", methods=['GET', 'POST'])
def contact_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_contact"
    id_contact_update = request.values['id_contact_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatecontact()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "contact_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_contact_update = form_update.nom_contact_update_wtf.data
            name_contact_update = name_contact_update.lower()

            valeur_update_dictionnaire = {"value_id_contact": id_contact_update, "value_name_contact": name_contact_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulecontact = """UPDATE t_contact SET nom_contact = %(value_name_contact)s WHERE id_contact = %(value_id_contact)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulecontact, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_contact_update"
            return redirect(url_for('contacts_afficher', order_by="ASC", id_contact_sel=id_contact_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_contact" et "nom_contact" de la "t_contact"
            str_sql_id_contact = "SELECT id_contact, nom_contact FROM t_contact WHERE id_contact = %(value_id_contact)s"
            valeur_select_dictionnaire = {"value_id_contact": id_contact_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_contact, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom contact" pour l'UPDATE
            data_nom_contact = mybd_curseur.fetchone()
            print("data_nom_contact ", data_nom_contact, " type ", type(data_nom_contact), " contact ",
                  data_nom_contact["nom_contact"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "contact_update_wtf.html"
            form_update.nom_contact_update_wtf.data = data_nom_contact["nom_contact"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans contact_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans contact_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans contact_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans contact_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("contacts/contact_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /contact_delete
    
    Test : ex. cliquer sur le menu "contacts" puis cliquer sur le bouton "DELETE" d'un "contact"
    
    Paramètres : sans
    
    But : Effacer(delete) un contact qui a été sélectionné dans le formulaire "contacts_afficher.html"
    
    Remarque :  Dans le champ "nom_contact_delete_wtf" du formulaire "contacts/contact_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/contact_delete", methods=['GET', 'POST'])
def contact_delete_wtf():
    data_films_attribue_contact_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_contact"
    id_contact_delete = request.values['id_contact_btn_delete_html']

    # Objet formulaire pour effacer le contact sélectionné.
    form_delete = FormWTFDeletecontact()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("contacts_afficher", order_by="ASC", id_contact_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "contacts/contact_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_contact_delete = session['data_films_attribue_contact_delete']
                print("data_films_attribue_contact_delete ", data_films_attribue_contact_delete)

                flash(f"Effacer le contact de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer contact" qui va irrémédiablement EFFACER le contact
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_contact": id_contact_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_contact = """DELETE FROM t_contact_contact WHERE fk_contact = %(value_id_contact)s"""
                str_sql_delete_idcontact = """DELETE FROM t_contact WHERE id_contact = %(value_id_contact)s"""
                # Manière brutale d'effacer d'abord la "fk_contact", même si elle n'existe pas dans la "t_contact_film"
                # Ensuite on peut effacer le contact vu qu'il n'est plus "lié" (INNODB) dans la "t_contact_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_contact, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idcontact, valeur_delete_dictionnaire)

                flash(f"contact définitivement effacé !!", "success")
                print(f"contact définitivement effacé !!")

                # afficher les données
                return redirect(url_for('contacts_afficher', order_by="ASC", id_contact_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_contact": id_contact_delete}
            print(id_contact_delete, type(id_contact_delete))

            # Requête qui affiche tous les films qui ont le contact que l'utilisateur veut effacer
            str_sql_contacts_films_delete = """SELECT id_contact, telephone_contact, mail_contact, nom_contact FROM t_contact_contact
                                            INNER JOIN t_contact ON t_contact.id_contact = t_contact_contact.fk_contact
                                            INNER JOIN t_contact ON t_contact.id_contact = t_contact_contact.fk_contact
                                            WHERE fk_contact = %(value_id_contact)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_contacts_films_delete, valeur_select_dictionnaire)
            data_films_attribue_contact_delete = mybd_curseur.fetchall()
            print("data_films_attribue_contact_delete...", data_films_attribue_contact_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "contacts/contact_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_contact_delete'] = data_films_attribue_contact_delete

            # Opération sur la BD pour récupérer "id_contact" et "nom_contact" de la "t_contact"
            str_sql_id_contact = "SELECT id_contact, nom_contact FROM t_contact WHERE id_contact = %(value_id_contact)s"

            mybd_curseur.execute(str_sql_id_contact, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom contact" pour l'action DELETE
            data_nom_contact = mybd_curseur.fetchone()
            print("data_nom_contact ", data_nom_contact, " type ", type(data_nom_contact), " contact ",
                  data_nom_contact["nom_contact"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "contact_delete_wtf.html"
            form_delete.nom_contact_delete_wtf.data = data_nom_contact["nom_contact"]

            # Le bouton pour l'action "DELETE" dans le form. "contact_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans contact_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans contact_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans contact_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans contact_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("contacts/contact_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_contact_delete)
