-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: zzz_xxxxx_NOM_PRENOM_INFO1X_SUJET_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS PROJECT_MACCAUD;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS PROJECT_MACCAUD;

-- Utilisation de cette base de donnée

USE PROJECT_MACCAUD;
-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Table structure for table `t_contact`
--

CREATE TABLE `t_contact` (
  `id_contact` int(11) NOT NULL,
  `telephone_contact` int(10) NOT NULL,
  `mail_contact` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_contact`
--

INSERT INTO `t_contact` (`id_contact`, `telephone_contact`, `mail_contact`) VALUES
(1, 794857087, 'jeremy.noth@epfl.ch'),
(2, 795454387, 'mattia.morrel@epfl.ch');

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_personne`
--

CREATE TABLE `t_contact_personne` (
  `id_contact_personne` int(11) NOT NULL,
  `fk_contact` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_personne` int(11) NOT NULL,
  `prenom_personne` varchar(42) NOT NULL,
  `nom_personne` varchar(42) NOT NULL,
  `date_naissance_personne` date NOT NULL,
  `genre_personne` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `prenom_personne`, `nom_personne`, `date_naissance_personne`, `genre_personne`) VALUES
(1, 'Jérémy', 'Noth', '2004-11-01', 'Homme'),
(2, 'Mattia', 'Morrel', '2003-11-14', 'Homme');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_contact`
--
ALTER TABLE `t_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `t_contact_personne`
--
ALTER TABLE `t_contact_personne`
  ADD PRIMARY KEY (`id_contact_personne`),
  ADD KEY `fk_contact` (`fk_contact`,`fk_personne`),
  ADD KEY `fk_personne` (`fk_personne`);

--
-- Indexes for table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_contact`
--
ALTER TABLE `t_contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_contact_personne`
--
ALTER TABLE `t_contact_personne`
  MODIFY `id_contact_personne` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_contact_personne`
--
ALTER TABLE `t_contact_personne`
  ADD CONSTRAINT `t_contact_personne_ibfk_1` FOREIGN KEY (`fk_contact`) REFERENCES `t_contact` (`id_contact`),
  ADD CONSTRAINT `t_contact_personne_ibfk_2` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`);
COMMIT;